Things to know:
1. Normal forms
2. Crow's foot diagram (how to **draw**)
3. Attributes, Entities, Methods
4. Create a diagram from **business rules**
5. SQL Queries - write out the result table
6. **Will be a time crunch**
