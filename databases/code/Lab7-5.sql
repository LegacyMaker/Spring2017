USE CS364; -- This database is from In-class lab7-4
################################################################
# See what tables are currently in that database
################################################################
SHOW TABLES;

ALTER TABLE salesperson
MODIFY COLUMN snum char(10);

DESCRIBE salesperson;
-- SELECT * FROM salesperson;

ALTER TABLE salesperson
MODIFY COLUMN comm DECIMAL(5,2);

ALTER TABLE salesperson
ADD COLUMN salecode CHAR(3);

ALTER TABLE salesperson
ADD COLUMN minitial char(1);

ALTER TABLE salesperson
DROP COLUMN minitial;

UPDATE salesperson
SET comm=0.25
WHERE snum=1007;

UPDATE salesperson
SET comm=0.17
WHERE snum=1004
AND city='London';

UPDATE salesperson
SET comm=comm + 0.05;

CREATE TABLE intern
(
      snum INTEGER NOT NULL,
      sname CHAR(15),
      city CHAR (15),
      comm NUMERIC(3,2)
 );
 
 
INSERT INTO Intern (snum, sname, city, comm)
                    SELECT snum, sname, city, comm FROM Salesperson;
                    
ALTER TABLE intern
MODIFY sname char(15) PRIMARY KEY;

DROP TABLE intern;

show tables;

