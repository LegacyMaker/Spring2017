use ch8;

# Step 2
SHOW TABLES;


-- Step 3
describe ticket;
select * from ticket where ticket_price > (SELECT avg(ticket_price) from ticket);

# Step 4
describe employee;
describe hours;

select distinct e.emp_fname, e.emp_lname
from employee e join hours h on (e.emp_num = h.emp_num)
where h.hour_rate > (select avg(hour_rate) from hours);

-- select * from employee;

# Step 5
describe employee;
describe themepark;

SELECT e.emp_num, e.emp_fname, e.emp_lname, p.park_name
from employee e join themepark p on (e.park_code = p.park_code)
where p.park_name like '%fairy%';

# Step 6
describe themepark;
describe sales_line;
describe sales;

select s.park_code, sum(line_qty) as tix_sold
from sales s join sales_line l on (s.transaction_no = l.transaction_no)
group by s.park_code
having tix_sold > (select avg(line_qty) from sales_line);

# Step 7
describe employee;
describe attraction;
describe hours;

select * from hours;

select distinct e.emp_fname, e.emp_lname, sum(hours_per_attract) as sum_hours
from employee e join hours h using(emp_num)
WHERE h.date_worked >= '2007-05-01'
AND h.date_worked <= '2007-05-31'
group by h.attract_no
having sum_hours > (max(hours_per_attract)); 

# Step 8
describe sales;
describe sales_line;
describe ticket;
describe themepark;

select ticket_no, park_code
from ticket
where ticket_price > (select max(ticket_price) from ticket where ticket_type='Child');

# Step 9
select ticket_no, (select avg(ticket_price) from ticket) as average,  ticket_price - (select avg(ticket_price) from ticket) as diff
 from ticket;
 
# Step 10
select transaction_no, line_no, line_qty, line_price
from sales_line s
where line_qty > (select avg(line_qty) from sales_line t where t.transaction_no=s.transaction_no);

# Step 11
describe themepark;
describe ticket;
describe sales_line;
describe sales;
select distinct t.park_code, park_name, park_country
from themepark join ticket t using(park_code) join sales_line using (ticket_no) join sales using(transaction_no)
where sale_date >= '2007-05-08';

