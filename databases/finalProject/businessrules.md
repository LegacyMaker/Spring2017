* A user's name, date of birth, and email need to be stored.
* A user may design any number of characters, and may also have any number of armies.
* A character must store its name, species, rank, faction, and the user that owns it.
* Characters also can take zero or more weapons, equipment, and skills. These three types of items work on an include/exclude table - the attributes a character has determines what is included and excluded in what the character can legally take.

health
strength
speed
command
will
skill
resistance
