#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>


int main(int argc, char*args[]) {
    pid_t  fork_rtn, getpid_rtn;
    fork_rtn = fork();
//    printf("%d\nWAIT\n", fork_rtn);
//    sleep(15);
    if(fork_rtn){
        wait(NULL);
    }
    getpid_rtn=getpid();
    printf("%d\n%d\n", fork_rtn, getpid_rtn);

    return 0;
}