#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <printf.h>

//Zombie
//int main() {
//    pid_t child_pid;
//
//    child_pid = fork();
//    if (child_pid > 0) {
//        sleep(30);  // sleep in parent will create a zombie
//                    // as the parent is not able
//                    // to receive signal that child has
//                    // terminated. <defunct> in ps aux
//        wait(NULL);
//    }
//    else {
//        exit(0);
//    }
//    return 0;
//}
//Orphan
//int main() {
//    pid_t child_pid;
//
//    child_pid = fork();
//    if (child_pid > 0) {
//        sleep(5);
//        exit(0);
//    }
//    else {
//        printf("Parent PID %d\n", getppid());
//        fflush(stdout);
//        sleep(30); // The parent will die during this interval
//        printf("Parent PID %d\n", getppid());
//        fflush(stdout);
//    }
//    return 0;
//}
//FORKBOMB
//int main() {
//    while(1){
//        fork();
//    }
//}