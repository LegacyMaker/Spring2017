#include <stdio.h>
#include <unistd.h>

int main() {

    //Example 2 - execvp() - execute a command
    char* argv[4] = {"ls", "-lF","-a", NULL};   // set up the argument list for execvp
                                                // it requires a char * array of pointers
                                                // to the command and it's arguments
                                                // this array of char * must end with a
                                                // NULL pointer

    printf("testing...execvp...\n");
    execvp(argv[0],argv);   // execvp requires the file name of the command first
                            // followed by the entire argument list to include the
                            // command itself
    printf("This should not print...\n");
    return 0;
}