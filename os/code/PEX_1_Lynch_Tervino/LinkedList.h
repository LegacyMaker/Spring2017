//
// Created by C19James.Lynch on 2/20/2018.
//

#ifndef PEX_1_LYNCH_TERVINO_LINKEDLIST_H
#define PEX_1_LYNCH_TERVINO_LINKEDLIST_H

typedef int ElementType;

typedef struct Node{
    char   data[64];
    struct Node* next;
    struct Node* prev;

} Node;


typedef struct linkedList
{

    int size;
    Node* first;
    Node*last;

}LinkedList;



void linkedListSelectionSort (LinkedList * list);
LinkedList * linkedListCreate();
void         linkedListDelete(LinkedList * list);
void         linkedListAppend(LinkedList *list, char *element);
void         linkedListPrint(LinkedList *list);
char *  linkedListGetElement(LinkedList *list, int position);
void         swapLinked(Node *value, Node *secondValue);
void         linkedListDeleteElement(LinkedList *list, int position);
void         linkedListInsertElement(LinkedList *list, int position, char *element);
void         linkedListChangeElement(LinkedList *list, int position, char *newValue);
int          linkedListFindElement(LinkedList *list, char *value);

#endif //PEX_1_LYNCH_TERVINO_LINKEDLIST_H
