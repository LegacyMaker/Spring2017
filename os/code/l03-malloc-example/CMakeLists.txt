cmake_minimum_required(VERSION 3.3)
project(Lsn04_Example)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c99 -Wall -Werror")

set(SOURCE_FILES main.c)
add_executable(Lsn04_Example ${SOURCE_FILES})