

A system has 20 bit logical address space and uses 8kb page sizes

What is the total logical memory space in MB?
2^20   = 1 MB
[reference](https://cs.stackexchange.com/questions/41931/size-of-address-spaces-logical-and-physical)


How many pages?
1MB/8KB = 1024 KB/8 KB = 128 pages

How many bits in the logical address are used for the page #? How many are used for the offset?
128 pages need **7 bits** to be represented, and 8 KB needs 13 bits
