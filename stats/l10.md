## Exponential
p(X<4) = `expondist(4, 1/5, 1)`

**Note that if in this problem one person arrives every 5 minutes, we put in a 1/5!!**

+ The exponential distribution **and only this one** has the *memoryless* property
1. Assume x~Expo(lambda)
  + P(X>t + s|X>t) = P(X>s)

**Type of problem**
You go to walmart and notice wait times are exponentially distributed with an average of six minutes. If you have been waiting in line for 3 minutes, what is the probability that your total wait is longer than 7 minutes?
