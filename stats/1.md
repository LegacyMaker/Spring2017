# Stats Lesson 1

## Introduction
+ Col. New *hates* lecturing. He will send out videos - five to ten minutes in length. Most of the class will be problems.
+ Willing to release us ten minutes early if you focus.
+ Teaches M4/6/7 - EI for the rest of that time. Set-up appointments.
+ Write your name on the white board if you need to go to the bathroom.
+ Col. New is more interested with practical application of the concepts more so than the theory ("not a mathecist")


## Homework and Quizzes
+ The first one is by far the longest.
+ WebAssign homepage is accesible via blackboard
+ On the syllabus, Quizzes have the lessons they cover in paranthesies.


## Course Outline
+ Probability, Inferential Statistics, and Variance and Regression are the three sections of the course.
+ Probability goes over how we predict certain outcomes.
+ Inferential Statistics is all about hypothesies and testing them. Likely the most tedious part of the course.
  + Answers the question "If I have a sample here, how can I make an educated guess about the whole population?"
+ Variance and Regression will talk about how much data varies. Sometimes we don't care about the exact numbers - we care more about variation.
  + Regression specifically refers to finding what factors are most important to a certain outcome (think Moneyball).

## Focused outcomes
+ 3, 5, 8, and 11. We're not gonna be quizzed by this.
